package model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
/**
 * 
 * @author Paul Siffre 
 * @author Antoine Gougault
 *
 */
public class Mot {
	
	private StringProperty titre;
	private StringProperty definition;
	
	/**
     * Constructeur parametré de la classe mot 
     * @param String, String
     */
	
	public Mot(String titre, String definition) {
		
		this.titre = new SimpleStringProperty(titre);
		this.definition = new SimpleStringProperty(definition);
		
		creerLexiNode(titre,definition);
	}

	/**
     * Getter de la valeur de titre
     * @return String
     */
	public String getTitre() {
		return titre.get();
	}
	
	/**
     * Getter de l'objet graphique titre
     * @return StringProperty
     */
	public StringProperty getTitreProperty() {
		return titre;
	}
	
	/**
     * Setter de la valeur de l'objet graphique titre
     * @param String 
     * @return void
     */
	public void setTitre(String titre) {
		this.titre.set(titre);
	}
	
	/**
     * Getter de la valeur de definition
     * @return String
     */
	public String getDefinition() {
		return definition.get();
	}
	
	/**
     * Getter de l'objet graphique definition
     * @param StringProperty
     */
	public StringProperty getDefinitionProperty() {
		return definition;
	}
	
	/**
     * Setter de la valeur de l'objet graphique definition
     * @param String 
     * @return void
     */
	public void setDefinition(String definition) {
		this.definition.set(definition);
	}
	
	/**
     * Methode d'affichade de la classe Mot
     * @return String
     */
	@Override
	public String toString() {
		return getTitre();
	}
	
	
	public void creerLexiNode(String titre, String definition) {
		int i =0;
		
	}
	
	
}
