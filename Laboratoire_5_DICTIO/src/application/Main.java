package application;
	
import java.io.File;
import java.io.IOException;

import controller.OverViewAppController;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import model.LectureEcriture;
import model.Mot;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
/**
 * 
 * @author Paul Siffre 
 * @author Antoine Gougault
 *
 */

public class Main extends Application {

	private Stage primaryStage;
    private BorderPane rootLayout;
    
    private File currentFile;
   
    private ObservableList<Mot> listDeMot = FXCollections.observableArrayList();

    /**
     * Metehode Main de l'application.
     * 
     * @param String[]
     * @return void
     */
    public static void main(String[] args) {

        launch(args);
    }
    
    /**
     * Metehode javaFx de d�marge de composant graphique.
     * 
     * @param Stage
     * @return void
     */
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("AddressApp");
        
        showOverViewApp();
    }
    
    /**
     * Permet de retourner le main stage.
     * @return Stage
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }
    
    /**
     * Methode permettant d'afficher la vue princiale ainsi que tout ses compossant graphique et sont controlleur.
     * @return void
     */
    public void showOverViewApp() {
    	try {
        	// Charge le Fxml de RootLayout.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/View/OverViewApp.fxml"));
            System.out.println(loader);
            rootLayout = (BorderPane) loader.load();
            
            OverViewAppController controller = loader.getController();
            System.out.println(controller);
            System.out.println(this);
            controller.setMainApp(this);
            
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Methode permettant de sauvegarder le dictionnaire actuelle dans un fichier.
     * @return void
     */
    public void saveToTxt() {
    	
    	LectureEcriture.ecrireFichier(currentFile.getAbsolutePath(), listDeMot);
	}
    
    /**
     * Methode permettant le dictionnaire actuelle depuis un fichier passer en param�tre.
     * @param File
     * @return void
     */
    public void LoadTxtFile(File file) {
    	    	
    	this.listDeMot = LectureEcriture.lireFichier(file.getAbsolutePath());
    }
    
    /**
     * Getter de la liste d�finition actuelle.
     * @return ObservableList<Mot>
     */
	public ObservableList<Mot> getlistDeMot() {
		return listDeMot;
	}
	
	/**
     * Setter de la liste d�finition actuelle.
     * @param ObservableList<Mot>
     * @return void
     */
	public void setlistDeMot(ObservableList<Mot> entrepriseData) {
		this.listDeMot = entrepriseData;
	}
	
	/**
     * Setter de la liste d�finition actuelle permettant d'ajouter un mot a la liste.
     * @param Mot
     * @return void
     */
	public void setMotInlistDeMot(Mot mot) {
		
		this.listDeMot.add(mot);
	}

	/**
     * Getter du fichier courrant.
     * @return File
     */
	public File getCurrentFile() {
		return currentFile;
	}
	
	/**
     * Setter du fichier courrant.
     * @param File
     * @return void
     */
	public void setCurrentFile(File currentFile) {
		this.currentFile = currentFile;
	}
	
}
