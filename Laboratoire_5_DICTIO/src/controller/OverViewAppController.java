package controller;

import java.io.File;

import application.Main;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import model.Mot;
import model.Recherche;
import javafx.scene.control.TextArea;
import javafx.scene.control.TableView;

public class OverViewAppController {
	@FXML
	private TextField TF_Search;
	@FXML
	private ListView<Mot> LV_QuickSearch;
	@FXML
	private Button BT_Save;
	@FXML
	private Button BT_Load;
	@FXML
	private TextArea TA_Definition;
	@FXML
	private Button BT_Add_Edit;
	@FXML
	private TableView<Mot> TV_WordList;
	@FXML
	private ListView<Mot> LV_WordList;

	
	private Main main;	
	private FileChooser chooser;
	private Mot mot;
	
	/**
     * Initialize le controller. La method est automatiquement appel�.
     * 
     * @return void
     */
    @FXML
    private void initialize() {
    	
    	chooser = new FileChooser();
    	    	
    	LV_WordList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showMotDetails(newValue));
    	LV_QuickSearch.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showMotDetails(newValue));
    	
    	TF_Search.textProperty().addListener((observable, oldValue, newValue) -> handleQuickSearch(newValue));
    }
    
    /**
     * Permet de configurer les liste a afficher du controlleur 
     * 
     * @param Main
     * @return void
     */
    public void setMainApp(Main main) {
    	
        this.main = main;
        
        LV_WordList.setItems(main.getlistDeMot());
        LV_QuickSearch.setItems(FXCollections.observableArrayList());
    }
	
    /**
	 *Permet de charger la liste de d�finition d'un fichier txt. 
	 * 
	 * @return void
	 */
	@FXML
	private void handleLoadFile() {		
		
		chooser.setInitialDirectory(new File("./"));
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Text files (*.txt)", "*.txt");
    	chooser.getExtensionFilters().add(extFilter);
    	File file = chooser.showOpenDialog(main.getPrimaryStage()) ;
		if (file != null) {
			
			main.setCurrentFile(file);
			main.LoadTxtFile(file);
			LV_WordList.setItems(main.getlistDeMot());
	    }
		
	}
	
	/**
	 *Permet de sauvegarder la liste de mot actuelle dans le fichier courrant.
	 *Si aucun fichier courrant n'a �t� selectionner, un nouveau fichier courrant et cr��. 
	 * 
	 * @return void
	 */
	@FXML
    private void handleSaveFile(){
				
		if (main.getCurrentFile() != null) {
			
			main.saveToTxt();
			
		} else {
			
			chooser.setInitialDirectory(new File("./"));
			FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Text files (*.txt)", "*.txt");
			chooser.getExtensionFilters().add(extFilter);
			File saveFile = chooser.showSaveDialog(main.getPrimaryStage()) ;
			if (saveFile != null) {
				
				File fileName = new File(saveFile.getAbsoluteFile().toString());
				
				if (fileName.exists()) {
					System.out.println("fileName.exists()");
					Alert alerteFichierExistant = new Alert(AlertType.CONFIRMATION);
					alerteFichierExistant.setTitle("Confirmer l'enregistrement");
					alerteFichierExistant.setHeaderText(saveFile.getAbsoluteFile() + ".txt existe d�j� \n" //alerteFichierExistant.setHeaderText(chooser.getInitialFileName() + ".csv existe d�j� \n"
						+ "Voulez-vous le remplacer ?");
					
					
					
					//Si la r�ponse de alerteFichierExistant est "CANCEL" alors...
					if(alerteFichierExistant.showAndWait().get() == ButtonType.CANCEL) {
						
						System.out.println("showAndWait() reponse annuler");
						return;
						
					}else {
					
						main.setCurrentFile(fileName);
						main.saveToTxt();
					}
						
				}else {
					
					main.setCurrentFile(fileName);
					main.saveToTxt();
				}
			}
		}
		
	}
	
	/**
	 *Permet de modifier un mot si il a �t� selectionn� dans la liste des mot ou la liste de recherche rapide. 
	 *Sinon un nouveau mot est cr�� et ajouter a la liste actuelle.
	 * 
	 * @return void
	 */
	@FXML
	private void handleNewEditFile() {
		
		if (TF_Search.getText().compareTo("") != 0) {
			
			if (LV_WordList.getSelectionModel().getSelectedItem() != null && TF_Search.getText().compareTo(LV_WordList.getSelectionModel().getSelectedItem().getTitre()) == 0) {
				
				LV_WordList.getSelectionModel().getSelectedItem().setDefinition(TA_Definition.getText());
			
			}else if(LV_QuickSearch.getSelectionModel().getSelectedItem() != null &&TF_Search.getText().compareTo(LV_QuickSearch.getSelectionModel().getSelectedItem().getTitre()) == 0) {
				
				LV_QuickSearch.getSelectionModel().getSelectedItem().setDefinition(TA_Definition.getText());
			}else {
				
				main.setMotInlistDeMot(new Mot(TF_Search.getText(), TA_Definition.getText()));
			}
		
		}
	}
	
	/**
	 * Remplie touts le text fields et le text area pour afficher un mot.
	 * Si mot est null, alors le text fields et le text area sont vide.
	 *
	 * @param Mot ou null
	 * @return void
	 */
	private void showMotDetails(Mot newValue) {
		try {
			
			this.mot = newValue;
		    if (mot != null) {
		        // Rempli les information d'mot
		    	TF_Search.setText(mot.getTitre());
		    	TA_Definition.setText(mot.getDefinition());
		    	
		    	
		    } else {
		        // mot is null, remove all the text. 	
		    	
		    	TA_Definition.setText("");
		    }
		    
		} catch (Exception e) {	}
		
	}
	
	private void handleQuickSearch(String text) {
		
		LV_QuickSearch.setItems(Recherche.rechercher(text, main.getlistDeMot()));
		
	}
	
}
