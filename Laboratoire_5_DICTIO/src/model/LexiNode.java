package model;

import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
/**
 * 
 * @author Paul Siffre 
 * @author Antoine Gougault
 *
 */
public class LexiNode {
	
	private char actualLetter;
	private StringProperty actualWord;
	private StringProperty deff;
	private  ObservableList<LexiNode> nextLetters = FXCollections.observableArrayList();
	
	/**
	 * 
	 * @param actualLetter lettre actuelle
	 * @param actualWord le mot actuel
	 * @param deff la definition
	 * @param nextLetters liste des lettres suivantes 
	 */
	public LexiNode(char actualLetter, StringProperty actualWord, StringProperty deff,
			ObservableList<LexiNode> nextLetters) {
		super();
		this.actualLetter = actualLetter;
		this.actualWord = actualWord;
		this.deff = deff;
		this.nextLetters = nextLetters;
	}
	/**
	 * getter actualLetter
	 * @return char actualLetter
	 */
	public char getActualLetter() {
		return actualLetter;
	}
/**
 * setter actualLetter
 * @param char actualLetter
 */
	public void setActualLetter(char actualLetter) {
		this.actualLetter = actualLetter;
	}
/**
 * getter actualWord
 * @return StringProperty actualWord
 */
	public StringProperty getActualWord() {
		return actualWord;
	}
/**
 * setter setActualWord
 * @param StringProperty actualWord
 */
	public void setActualWord(StringProperty actualWord) {
		this.actualWord = actualWord;
	}
/**getter deff
 * 
 * @return StringProperty deff
 */
	public StringProperty getDeff() {
		return deff;
	}
/**
 * setter deff
 * @param StringProperty deff
 */
	public void setDeff(StringProperty deff) {
		this.deff = deff;
	}


	/**
	 * retourne la liste des lettres suivantes
	 * getter nextLetters
	 * @return ObservableList<LexiNode> nextLetters
	 */
	public ObservableList<LexiNode> getNextLetters() {
		return nextLetters;
	}
/**
 * set la liste des lettres suivantes
 * @param ObservableList<LexiNode>  nextLetters
 */
	public void setNextLetters(ObservableList<LexiNode> nextLetters) {
		this.nextLetters = nextLetters;
	}

	/**
	 *FONCTION RECURCIVE 
	 *
	 * fonction de recherche dans l'arbre creer par les LexiNodes 
	 * @param String recherche le mot rechercher
	 * @param int nbappel METTRE LA VALEUR A 0 LORS DE L APPEL
	 * @param ObservableList<LexiNode> quickSearchDeMot
	 * @return ObservableList<LexiNode> liste des lettres suivantes
	 */
	public  ObservableList<LexiNode> rechercher (String recherche, int nbappel,ObservableList<LexiNode> quickSearchDeMot) {
		 ObservableList<LexiNode> temp = FXCollections.observableArrayList();
		if(nbappel< recherche.length()) {
			for (LexiNode entry :quickSearchDeMot) {
				if(entry.actualLetter == recherche.toCharArray()[nbappel]) {
					temp.add(entry);
					rechercher(recherche,nbappel +1,temp);
				}
			}
		}
		return temp;
	}
}
