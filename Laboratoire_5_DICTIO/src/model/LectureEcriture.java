package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
/**
 * 
 * @author Paul Siffre 
 * @author Antoine Gougault
 *
 */
public class LectureEcriture {
	/**
	 * lit le fichier selectionne et enregistre son contenu dans une map de mot
	 * @param String path du fichier
	 * @return ObservableList<Mot>
	 **/
	public static ObservableList<Mot> lireFichier(String path){
		ObservableList<Mot> temp = FXCollections.observableArrayList();
		try{
			InputStream  file =  new FileInputStream(path);
			BufferedReader buff =new BufferedReader(new InputStreamReader(file , "UTF8"));
			String ligne;
			while ((ligne=buff.readLine())!=null){
				String[] ligneCut = ligne.split(" & ");
				temp.add(new Mot(ligneCut[0],ligneCut[1]));
				System.out.println(new Mot(ligneCut[0],ligneCut[1]).toString());
			}
			buff.close();
			file.close();
		}catch (Exception e){
			System.out.println(e.toString());
		}
		return temp;
	}
	/**
	 * @param String path pour ecrire le fichier
	 * @param ObservableList<Mot> list liste des mots
	 */
	public static void ecrireFichier(String path, ObservableList<Mot> list) {
        final File file =new File(path); 
        try {
        	file .createNewFile();
            PrintWriter writer = new PrintWriter(file,"UTF-8");
            for (Mot entry :list) {
            	writer.println(entry.getTitre() + " & " + entry.getDefinition());
    	    }
                writer.close();
        } catch (Exception e) {
            System.out.println("Impossible de creer le fichier");
        }
	}
	
}
