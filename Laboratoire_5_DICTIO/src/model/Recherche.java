package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
/**
 * 
 * @author Paul Siffre 
 * @author Antoine Gougault
 *
 */
public class Recherche {

	/**
	 * 
	 * @param String mot mot recherche
	 * @param ObservableList<Mot> ListMots liste des mots disponible
	 * @return ObservableList<Mot> liste des mots correspondants
	 */
	public static ObservableList<Mot> rechercher(String mot,ObservableList<Mot> ListMots){
		
	  ObservableList<Mot> temp = FXCollections.observableArrayList();
	  ObservableList<Mot> temp2 = FXCollections.observableArrayList();
	  
	    for (Mot entry : ListMots) {
        	if(entry.getTitre().length() >= mot.length()) {
        		temp.add(entry);
        	}
        	
        }
	    Boolean isMatch = false;
	    for (Mot entry : temp) {
	    	for(int i = 0; i < mot.length(); i++) {
	        	if(entry.getTitre().toCharArray()[i] == mot.toCharArray()[i]) {
	        		isMatch = true;
	        	}else {
	        		isMatch = false;
	        	}
	    	}
	    	if(isMatch){
	    		temp2.add(entry);
	    	}
	    	
	    }
	    return temp2;
	};
}
